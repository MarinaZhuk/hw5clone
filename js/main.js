function copy() {
    let arr = arguments[0];
    
    for (let i = 1; i < arguments.length; i++) {
        let  arg = arguments[i];
        for (let key in arg) {
            arr[key] = arg[key];
        }
    }
    return arr;
}